package fr.imtld.sdl;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_OVERLAYPeer;
import com.sun.xml.internal.ws.api.SOAPVersion;
import com.sun.xml.internal.ws.policy.jaxws.SafePolicyReader;
import fr.imtld.sdl.support.Fifo;
import fr.imtld.sdl.support.IProcess;
import fr.imtld.sdl.support.Signal;

/* *************************************************************************
* Le code SDL textuel pour le process Pipe de la sp�cification du crible
* d'Erathost�ne.
* Conseil : copier / coller des parties de cette sp�cification comme
* commentaire du code Java qui implante le comportement correspondant �
* chaque partie comme si un g�n�rateur avait produit le code Java.

* Ne pas formater le code de tout le fichier pour ne pas perdre la mise en
* forme qui suit !

PROCESS Pipe(1,);
  SIGNALSET SNb, SPrime, SKill, SOver;
  DCL p, n integer;
  DCL prec PID;

  START ;
    NEXTSTATE Initial;

  STATE Initial ;
    INPUT SNb(p);
      TASK prec := sender;
      OUTPUT SPrime(p) TO prec;
      NEXTSTATE First;
  ENDSTATE;

  STATE First;
    INPUT SNb( n );
      DECISION n mod p;
        ( 0 ):
          NEXTSTATE -;
        ELSE:
          CREATE Pipe;
  lblSNb :
          OUTPUT SNb( n ) TO offspring;
          NEXTSTATE Child;
      ENDDECISION;
    INPUT SKill;
  lblStop:
      OUTPUT SOver TO prec;
      DECISION parent;
        ( null ):
          NEXTSTATE Initial;
        ELSE:
          STOP ;
      ENDDECISION;
  ENDSTATE;

  STATE Child
    COMMENT 'offspring existe !';
    INPUT SNb( n );
      DECISION n mod p;
        ( 0 ):
          NEXTSTATE -;
        ELSE:
          JOIN lblSNb;
      ENDDECISION;
    INPUT SPrime( n );
      OUTPUT SPrime( n ) to prec;
      NEXTSTATE -;
    INPUT SKill;
      OUTPUT SKill TO offspring;
      NEXTSTATE -;
    INPUT SOver;
      JOIN lblStop;
  ENDSTATE;
ENDPROCESS;
***************************************************************************/

/**
 * Implante le process SDL Pipe (un �tage de pipeline d'Erathosth�ne) ;
 * IProcess correspond au type PId de SDL.
 */
public class Pipe implements IProcess {

	/* Implantation des concepts SDL. */
	/**
	 * Variable d'�tat de l'automate mod�lisant le comportement du process Pipe.
	 */
    private int state;
    /** Thread pour une instance de process SDL. */
    private Thread _thread;
    /** Fifo pour une instance de process SDL. */
    private Fifo _fifo;
    /** Contr�le la terminaison de l'instance de process SDL. */
    private boolean _bRun;

    /* Implantation des expressions SDL pr�d�finies ; TODO penser � leur donner
     * une valeur aux moments appropri�s ! */
    
    /** Implantation de l'expression SDL parent. */
    private IProcess parent;
    /** Implantation de l'expression SDL offspring. */
    private IProcess offspring;
    /** Implantation de l'expression SDL sender. */
    private IProcess sender;
    /** Implantation de l'expression SDL self. */
    private IProcess self;

    /* Implantation des variables SDL du process Pipe. */

    /** Etats du process Pipe. */
    private static final int INITIAL=0, FIRST=1, CHILD=2;
    /** Implantation de DCL n integer; */
    private int n;
    /** Implantation de DCL p integer; */
    private int p;
    /** Implantation de DCL prec PId; */
    private IProcess prec;
	/**
	 * Runnable pour le point d'entr�e du thread de l'instance de process SDL.
	 */
    protected Runnable runnable = new Runnable() {
    	/**
    	 * Point d'entr�e du thread de l'instance de process SDL.
    	 */
	    public void run() {
		// TODO coder une boucle :
		//   Attendre que la fifo soit non vide
		//   Obtenir un signal de la fifo
	    //   selon l'�tat courant, aiguiller vers une m�thode correspondante (� �crire) :
	    //     INITIAL -> initial()
	    //     FIRST -> first()
	    //     CHILD -> child()
	    //   Dans chaque m�thode/�tat, aiguiller ensuite selon le signal re�u en se
	    //   laissant guider par les INPUT de la sp�cification en SDL textuel.
	    //   Conseil : sous chaque ligne de SDL textuel, �crire le code Java correspondant
	    //   comme si un g�n�rateur avait produit ce code Java.
		// fin de boucle

            while(_bRun == true) {
                if(!_fifo.isEmpty()){
                    Signal signal = (Signal) _fifo.get();
                    switch(state){
                        case 0:
                            initial(signal);
                            break;
                        case 1:
                            first(signal);
                            break;
                        case 2:
                            child(signal);
                            break;
                    }
                }
            }
	    }

	    private void initial(Signal signal){
	        if(signal instanceof SNb){
                prec = signal.getSender();
                prec.add(new SPrime(self, ((SNb) signal).nb()));
                p = ((SNb) signal).nb();
            }
            state = 1;
        }

        private void first(Signal signal){
            if(signal instanceof SNb){
                n = ((SNb) signal).nb();
                if(!((n % p) == 0)){
                    offspring = new Pipe();
                    offspring.setParent(self);
                    offspring.add(signal);
                    state = 2;
                }
            }
            if(signal instanceof SKill){
                prec.add(new SOver(self));
                if(parent == null){
                    state = 0;
                }
                else{
                    _bRun = false;
                }
            }
        }

        private void child(Signal signal){
            if(signal instanceof SNb){
                n = ((SNb) signal).nb();
                if(!((n % p) == 0)){
                    offspring.add(signal);
                }
            }
            if(signal instanceof SPrime){
                prec.add(signal);
            }
            if(signal instanceof SKill){
                offspring.add(signal);
            }
            if(signal instanceof SOver){
                prec.add(new SOver(self));
                if(parent == null){
                    state = 0;
                }
                else{
                    _bRun = false;
                }
            }
        }
    };

    public Pipe() {
        this.self = this;
    	this.state = 0;
    	this._fifo = new Fifo();
    	this._bRun = true;
    	this._thread = new Thread(runnable);
    }

	/**
	 * D�poser un signal dans la fifo de cette instance de process SDL.
	 * 
	 * @param signal
	 *            Le signal � d�poser.
	 */
    public void add( Object signal ) {
    	_fifo.add(signal);
    }

	/**
	 * D�finir le parent de ce Pipe et d�marrer son thread SDL.
	 * 
	 * @param procParent
	 *            Processus parent
	 */
    public void setParent( IProcess procParent ) {
        parent = procParent;
        _thread.start();
    }

    /*
     * Terminer l'instance de process SDL (support pour la construction SDL
     * STOP).
     */
    private void stop() {
    	this._bRun = false;
    }
}
